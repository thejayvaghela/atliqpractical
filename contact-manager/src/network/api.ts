import axios from "axios";

const timeoutDuration = 10 * 1000;

const makeCall = axios.create({ timeout: timeoutDuration });

export default makeCall;
