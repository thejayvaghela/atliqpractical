import { get, seed } from "global/constants/strings";
import { baseURL } from "global/constants/urls";
import { Filters, User } from "interfaces/interfaces";
import { getNatFilters } from "utils/methods";
import { getCallParams } from "utils/service";
import makeCall from "./api";

export async function getAllUsers(
  page: number,
  results: number,
  gender: Filters,
  nationality: Filters[]
) {
  try {
    let params;
    const natFilters = getNatFilters(nationality);
    if (natFilters !== "")
      params = {
        page,
        results,
        seed,
        gender: gender.label.toLowerCase(),
        nationality: natFilters,
      };
    else params = { page, results, seed, gender: gender.label.toLowerCase() };

    const callParams = getCallParams(get, baseURL, params);

    const response = await makeCall(callParams);
    return response.data;
  } catch (e: any) {
    console.log("e == ", e);
    return [];
  }
}
