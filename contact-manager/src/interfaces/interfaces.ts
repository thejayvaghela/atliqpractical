export interface User {
  cell: string;
  dob: Dob;
  email: string;
  gender: string;
  id: ID;
  location: Location;
  name: Name;
  nat: string;
  phone: string;
  picture: Picture;
}

export interface FavUser {
  uniqueId: string;
  user: User;
}

export interface Name {
  first: string;
  last: string;
  title: string;
}

export interface Picture {
  large: string;
  medium: string;
  thumbnail: string;
}

export interface Dob {
  age: number;
  date: string;
}

export interface ID {
  name: string;
  value: string;
}

export interface Location {
  city: string;
  coordinates: Coordinates;
  country: string;
  postcode: number;
  state: string;
  street: Address;
  timezone: TimeZone;
}

export interface TimeZone {
  description: string;
  offset: string;
}

export interface Address {
  name: string;
  number: number;
}

export interface Coordinates {
  latitude: string;
  longitude: string;
}

export interface Filters {
  type: string;
  label: string;
}
