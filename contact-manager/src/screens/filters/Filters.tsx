import { genders, nationalities } from "global/constants/strings";
import { Filters } from "interfaces/interfaces";
import React from "react";
import { View, Text } from "react-native";
import { IconButton, List } from "react-native-paper";

interface CustomProps {
  genderFilters: Filters;
  natFilters: Filters[];
  setGenderFilters: Function;
  setNatFilters: Function;
}
const Filter = (props: CustomProps) => {
  let genderFilters = props.genderFilters;
  let natFilters = props.natFilters;

  const handleFilter = (type: string, label: string) => {
    if (type === "gender") props.setGenderFilters(label);
    if (type === "nationality") {
      if (natFilters.includes({ type, label })) {
        const index = natFilters.indexOf({ type, label });
        natFilters.splice(index, 1);
      } else {
        natFilters.push({ type, label });
      }
      props.setNatFilters(natFilters);
    }
  };

  return (
    <List.Section>
      <List.Accordion title="Gender">
        {genders.map((gender, key) => {
          return (
            <List.Item
              key={key}
              title={gender}
              right={() => {
                if (genderFilters.label === gender)
                  return <IconButton icon="check" />;
              }}
              onPress={() => handleFilter("gender", gender)}
            />
          );
        })}
      </List.Accordion>
      <List.Accordion title="Nationality">
        {nationalities.map((nat, key) => {
          return (
            <List.Item
              key={key}
              title={nat}
              right={() => {
                if (natFilters.includes({ type: "nationality", label: nat }))
                  return <IconButton icon="check" />;
              }}
              onPress={() => handleFilter("nationality", nat)}
            />
          );
        })}
      </List.Accordion>
    </List.Section>
  );
};

export default Filter;
