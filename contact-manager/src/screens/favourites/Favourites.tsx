import { User } from "interfaces/interfaces";
import React from "react";
import { View, Text, SafeAreaView, FlatList } from "react-native";
import { useAppSelector } from "redux/store";
import UserItem from "screens/userItem/UserItem";
import styles from "screens/userList/UserList.styles";

interface CustomProps {
  navigation: any;
}
const Favourites = (props: CustomProps) => {
  const favList = useAppSelector((state) => state.favList.list);
  const loading = useAppSelector((state) => state.favList.loading);

  if (loading) return <Text>Loading</Text>;

  return (
    <SafeAreaView style={styles.container}>
      <View style={{ width: "100%" }}>
        <FlatList
          data={favList}
          keyExtractor={(item: User, index) => item.email + index}
          // showsVerticalScrollIndicator={false}
          renderItem={({ item, index }: { item: User; index: number }) => (
            <UserItem
              user={item}
              navigation={props.navigation}
              favList={favList}
            />
          )}
        />
      </View>
    </SafeAreaView>
  );
};

export default Favourites;
