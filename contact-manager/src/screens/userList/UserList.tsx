import { Filters, User } from "interfaces/interfaces";
import { getAllUsers } from "network/users";
import React, { useEffect, useState } from "react";
import { View, Text, SafeAreaView } from "react-native";
import { FlatList, TouchableOpacity } from "react-native-gesture-handler";
import { DataTable } from "react-native-paper";
import { useAppDispatch, useAppSelector } from "redux/store";
import { getUsers } from "redux/userSlice";
import Filter from "screens/filters/Filters";
import UserItem from "screens/userItem/UserItem";
import styles from "./UserList.styles";

interface CustomProps {
  navigation: any;
}
const UserList = (props: CustomProps) => {
  const numberOfItemsPerPageList = [20, 30, 50];

  props.navigation.setOptions({
    headerRight: () => (
      <TouchableOpacity
        onPress={() => {
          props.navigation.navigate("Favourites");
        }}
      >
        <Text>Favourites</Text>
      </TouchableOpacity>
    ),
  });

  const dispatch = useAppDispatch();

  const userList = useAppSelector((state) => state.userList.list);
  const favList = useAppSelector((state) => state.favList.list);
  const loading = useAppSelector((state) => state.userList.loading);

  const [page, setPage] = useState(0);
  const [numberOfItemsPerPage, onItemsPerPageChange] = useState(
    numberOfItemsPerPageList[0]
  );
  const from = page * numberOfItemsPerPage;
  const to = Math.min((page + 1) * numberOfItemsPerPage, userList.length);

  const [showFilters, setShowFilters] = useState(false);

  const [genderFilters, setGenderFilters] = useState<Filters>({
    label: "",
    type: "gender",
  });
  const [natFilters, setNatFilters] = useState<Filters[]>([]);

  useEffect(() => {
    dispatch(
      getUsers({
        pageNo: page + 1,
        noOfItemPerPage: numberOfItemsPerPage,
        gender: genderFilters,
        nationality: natFilters,
      })
    );
  }, [numberOfItemsPerPage, page, genderFilters.label, natFilters]);

  const applyGender = (label: string) => {
    setGenderFilters({ ...genderFilters, label });
  };
  const applyNat = (filters: Filters[]) => {
    setNatFilters(filters);
  };

  const toggleFilters = () => {
    setShowFilters(!showFilters);
  };

  if (loading)
    return (
      <View style={styles.container}>
        <Text>Loading</Text>
      </View>
    );

  return (
    <SafeAreaView style={styles.container}>
      <View>
        <TouchableOpacity onPress={toggleFilters}>
          <Text>Show Filters</Text>
        </TouchableOpacity>
        {showFilters && (
          <Filter
            genderFilters={genderFilters}
            natFilters={natFilters}
            setGenderFilters={applyGender}
            setNatFilters={applyNat}
          />
        )}
        <FlatList
          data={userList}
          keyExtractor={(item: User, index) => item.email + index} // ids are not unique.
          // showsVerticalScrollIndicator={false}
          renderItem={({ item, index }: { item: User; index: number }) => (
            <UserItem
              user={item}
              navigation={props.navigation}
              favList={favList}
            />
          )}
        />
        <DataTable>
          <DataTable.Pagination
            page={page}
            numberOfPages={10000}
            onPageChange={(page) => {
              setPage(page);
            }}
            label={`Page : ${page + 1}`}
            showFastPaginationControls
            numberOfItemsPerPageList={numberOfItemsPerPageList}
            numberOfItemsPerPage={numberOfItemsPerPage}
            onItemsPerPageChange={onItemsPerPageChange}
            selectPageDropdownLabel={"Results per page"}
          />
        </DataTable>
      </View>
    </SafeAreaView>
  );
};

export default UserList;
