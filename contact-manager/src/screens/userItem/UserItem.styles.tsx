import { StyleSheet } from "react-native";
const styles = StyleSheet.create({
  container: {
    margin: 5,
    flexDirection: "row",
    // width: "100%",
    borderWidth: 0.4,
    flex: 1,
  },
  textCont: {
    justifyContent: "center",
    flex: 1,
  },
});

export default styles;
