import { User } from "interfaces/interfaces";
import React, { useEffect, useState } from "react";
import { View, Text } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { IconButton } from "react-native-paper";
import { addRemoveFav } from "redux/favouriteSlice";
import { useAppDispatch } from "redux/store";
import { getFavIcon, isUserFav } from "utils/methods";
import styles from "./UserItem.styles";

interface CustomProps {
  user: User;
  navigation: any;
  favList: User[];
}
const UserItem = (props: CustomProps) => {
  const user = props.user;
  const favList = props.favList;
  const fullName = `${user.name.title} ${user.name.first} ${user.name.last}`;
  const [isFav, setIsFav] = useState(false);

  const dispatch = useAppDispatch();

  useEffect(() => {
    setIsFav(isUserFav(user));
  }, [favList]);

  const handleSelectPress = () => {
    props.navigation.navigate("UserDetails", {
      user: user,
      fullName: fullName,
    });
  };

  const handleFavPress = () => {
    dispatch(addRemoveFav({ user }));
  };

  return (
    <View style={styles.container}>
      <View style={styles.textCont}>
        <TouchableOpacity onPress={handleSelectPress}>
          <Text>{fullName}</Text>
          <Text>{user.cell}</Text>
        </TouchableOpacity>
      </View>
      <IconButton
        onPress={handleFavPress}
        icon={getFavIcon(isFav)}
        style={{ marginLeft: "auto" }}
      />
    </View>
  );
};

export default UserItem;
