import { User } from "interfaces/interfaces";
import React from "react";
import { View, Text } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import { Avatar, IconButton } from "react-native-paper";
import { addRemoveFav } from "redux/favouriteSlice";
import { useAppDispatch, useAppSelector } from "redux/store";
import styles from "./UserDetails.styles";
import { getFavIcon, isUserFav } from "./../../utils/methods";

interface CustomProps {
  navigation: any;
  route: any;
}
const UserDetails = (props: CustomProps) => {
  const dispatch = useAppDispatch();

  const favList = useAppSelector((state) => state.favList.list);

  const user: User = props.route.params.user;
  const fullName: string = props.route.params.fullName;

  const isFav = isUserFav(user);

  const handleFavPress = () => {
    dispatch(addRemoveFav({ user }));
  };

  props.navigation.setOptions({
    title: fullName,
    headerLeft: () => {
      return (
        <IconButton
          icon="chevron-left"
          onPress={() => {
            props.navigation.goBack();
          }}
        />
      );
    },
    headerRight: () => {
      return <IconButton icon={getFavIcon(isFav)} onPress={handleFavPress} />;
    },
  });

  return (
    <ScrollView>
      <View style={styles.picture}>
        <Avatar.Image size={200} source={{ uri: user.picture.large }} />
      </View>
      <View style={styles.details}>
        <Text>Name : {fullName}</Text>
        <Text>Cell : {user.cell}</Text>
        <Text>DOB : {user.dob.date}</Text>
        <Text>Gender : {user.gender}</Text>
        <Text>Nationality : {user.nat}</Text>
        <Text>Phone : {user.phone}</Text>
      </View>
    </ScrollView>
  );
};

export default UserDetails;
