import { StyleSheet } from "react-native";
const styles = StyleSheet.create({
  // container: {
  //   width: "100%",
  //   margin: 5,
  //   flexDirection: "row",
  //   borderWidth: 0.4,
  //   flex: 1,
  // },
  picture: {
    width: "100%",
    alignItems: "center",
  },
  details: {
    margin: 10,
    flex: 1,
    alignItems: "center",
  },
});

export default styles;
