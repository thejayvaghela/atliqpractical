import { AxiosError, Method } from "axios";
import { applicationJSON } from "global/constants/strings";
import { Alert } from "react-native";
// import store from "redux/store";

export function getCallParams(
  method: Method,
  url: string,
  params?: any,
  body?: any
) {
  return {
    baseURL: url,
    method: method,
    data: body,
    params: params,
  };
}
