import { Filters, User } from "interfaces/interfaces";
import store from "redux/store";

export const isUserFav = (user: User) => {
  const favList = store.getState().favList.list;
  if (favList.includes(user)) return true;
  return false;
};

export const getFavIcon = (isFav: boolean) => {
  return isFav ? "heart" : "heart-outline";
};

export const getNatFilters = (filters: Filters[]) => {
  let natList = "";
  filters.map((nat) => {
    natList = natList + nat.label.toLowerCase() + ",";
  });
  return natList;
};
