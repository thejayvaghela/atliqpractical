import { configureStore } from "@reduxjs/toolkit";
import { TypedUseSelectorHook, useDispatch, useSelector } from "react-redux";
import favReducer from "./favouriteSlice";
import userReducer from "./userSlice";

const store = configureStore({
  reducer: {
    userList: userReducer,
    favList: favReducer,
  },
});

export default store;

export type RootState = ReturnType<typeof store.getState>;

export const useAppDispatch = () => useDispatch<typeof store.dispatch>();
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
