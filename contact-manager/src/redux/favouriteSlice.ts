import AsyncStorage from "@react-native-async-storage/async-storage";
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { FAVUSERS } from "global/constants/asyncStorage";
import { User } from "interfaces/interfaces";
import { cloneDeep } from "lodash";
import { RootState } from "./store";

interface State {
  loading: boolean;
  list: User[];
}
const initialState: State = {
  loading: true,
  list: [],
};

export const initialLoad = createAsyncThunk<User[], void, { state: RootState }>(
  "favourite/initialLoad",
  async (thunkAPI) => {
    const favUsers = await AsyncStorage.getItem(FAVUSERS);
    let favList: User[];
    if (favUsers) favList = JSON.parse(favUsers);
    return favList!;
  }
);

export const addRemoveFav = createAsyncThunk<
  User[],
  { user: User },
  { state: RootState }
>("favourite/addRemoveFav", async ({ user }, thunkAPI) => {
  let list = cloneDeep(thunkAPI.getState().favList.list);

  if (list.indexOf(user) >= 0) {
    const index = list.indexOf(user);
    list.splice(index, 1);
  } else {
    list.push(user);
  }
  AsyncStorage.setItem(FAVUSERS, JSON.stringify(list));

  return list;
});

const favouriteSlice = createSlice({
  name: "favourite",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(addRemoveFav.fulfilled, (state, action) => {
      state.list = action.payload;
      state.loading = false;
    });
    builder.addCase(initialLoad.fulfilled, (state, action) => {
      if (action.payload === null || action.payload === undefined) return;
      state.list = action.payload;
      state.loading = false;
    });
  },
});

export default favouriteSlice.reducer;
