// import AsyncStorage from "@react-native-async-storage/async-storage";
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { defaultPage, defaultResults } from "global/constants/strings";
import { Filters, User } from "interfaces/interfaces";
import { getAllUsers } from "network/users";
import { RootState } from "./store";
import { nationalities } from "global/constants/strings";

interface State {
  loading: boolean;
  list: User[];
  filters: Filters[];
}
const initialState: State = {
  loading: true,
  list: [],
  filters: [],
};

export const getUsers = createAsyncThunk<
  any,
  {
    pageNo: number;
    noOfItemPerPage: number;
    gender: Filters;
    nationality: Filters[];
  },
  { state: RootState }
>(
  "user/getUsers",
  async ({ pageNo, noOfItemPerPage, gender, nationality }, thunkAPI) => {
    return getAllUsers(pageNo, noOfItemPerPage, gender, nationality);
  }
);

const favouriteSlice = createSlice({
  name: "favourite",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getUsers.fulfilled, (state, action) => {
      const results = action.payload.results;
      state.list = results;
      state.loading = false;
    });
  },
});

export default favouriteSlice.reducer;
