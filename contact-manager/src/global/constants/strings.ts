import { Method } from "axios";

export const get: Method = "GET";
export const post: Method = "POST";
export const put: Method = "PUT";
export const deleteCall: Method = "DELETE";

export const applicationJSON = { "Content-Type": "application/json" };

export const defaultPage = 1;
export const defaultResults = 50;
export const seed = "abc";

export const genders = ["Male", "Female"];

export const nationalities = ["AU", "BR", "CA", "CH", "DE", "GB", "NZ", "US"];
