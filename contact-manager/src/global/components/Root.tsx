import React, { useEffect } from "react";
import { View, Text } from "react-native";
import { Provider as ReduxProvider } from "react-redux";
import { Provider as PaperProvider } from "react-native-paper";
import store from "redux/store";
import Navigator from "global/components/Navigator";
import { NavigationContainer } from "@react-navigation/native";

const Root = () => {
  return (
    <ReduxProvider store={store}>
      <PaperProvider>
        <NavigationContainer>
          <Navigator />
        </NavigationContainer>
      </PaperProvider>
    </ReduxProvider>
  );
};

export default Root;
