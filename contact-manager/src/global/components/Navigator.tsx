import { NavigationContainer, useNavigation } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import React, { useEffect } from "react";
import { View, Text } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { initialLoad } from "redux/favouriteSlice";
import { useAppDispatch } from "redux/store";
import Favourites from "screens/favourites/Favourites";
import UserDetails from "screens/userDetails/UserDetails";
import UserList from "screens/userList/UserList";

const Stack = createStackNavigator();

const Navigator = () => {
  const navigation = useNavigation();

  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(initialLoad());
  }, []);

  return (
    <Stack.Navigator initialRouteName="UserList">
      <Stack.Screen
        name="UserList"
        component={UserList}
        options={{ title: "Contact List" }}
      />
      <Stack.Screen name="UserDetails" component={UserDetails} />
      <Stack.Screen
        name="Favourites"
        component={Favourites}
        options={{ title: "Favourites" }}
      />
    </Stack.Navigator>
  );
};

export default Navigator;
