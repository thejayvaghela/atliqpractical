import { StatusBar } from "expo-status-bar";
import Root from "global/components/Root";
import React from "react";

export default function App() {
  return <Root />;
}
